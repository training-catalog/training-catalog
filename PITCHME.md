## Training catalog
### Vertical Sysadmin, Inc.
#### hello@verticalsysadmin.com +1-323-393-0779

- Git 
- Shell
- GitLab 
- Jenkins
- Vim
- C/C++
- Time Management
- Leadership
- Troubleshooting
- Puppet
- CFEngine
- Docker
- Kubernetes
